import { NgModule } from '@angular/core';
import { StudentPage } from '@talentlabs/app/app/modules/student/pages/student.page';
import { StudentService } from '@talentlabs/app/services/student.service';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatRippleModule } from '@angular/material/core';
import { StudentCardModule } from '@talentlabs/app/app/modules/student/components/cards/student/module';
import { CommonModule } from '@angular/common';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { StudentFormModule } from '@talentlabs/app/app/modules/student/components/forms/student-form/module';

@NgModule({
    declarations: [
        StudentPage
    ],
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: StudentPage,
            },
        ]),
        MatButtonModule,
        MatRippleModule,
        StudentCardModule,
        CommonModule,
        MatDialogModule,
        MatIconModule,
        StudentFormModule,
    ],
    providers: [StudentService],
})
export class StudentModule {
}
