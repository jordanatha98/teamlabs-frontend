import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Course } from '@talentlabs/app/models/course';
import { Batch } from '@talentlabs/app/models/batch';
import { map, startWith } from 'rxjs';
import { StudentService } from '@talentlabs/app/services/student.service';

@Component({
    selector: 'student-form',
    templateUrl: './student.form.html',
    styleUrls: ['./student.form.scss']
})
export class StudentForm implements OnInit {
    @Input() public courses: Course[] | null | undefined
    @Input() public batches: Batch[] | null | undefined;
    @Output() public submit: EventEmitter<any> = new EventEmitter<any>();

    public filteredCourses: any = [];
    public filteredBatches: any = [];

    //creating the form
    public form = this.fb.group({
        students: this.fb.array([])
    })

    public constructor(private fb: FormBuilder, private cdRef: ChangeDetectorRef, private service: StudentService) {
    }

    // function for adding student with the validators
    public addStudent(): void {
        const controls = <FormArray>this.form.controls['students'];
        const studentForm = this.fb.group({
            email: new FormControl('', Validators.required),
            fee: new FormControl(''),
            course: new FormControl(''),
            batch: new FormControl(''),
        });

        this.students.push(studentForm);
        this.manageCourseControl(controls.length - 1);
        this.manageBatchControl(controls.length - 1);
    }

    public manageCourseControl(index: number) {
        const arrayControl = this.form.get('students') as FormArray;
        if (arrayControl) {
            this.filteredCourses[index] = arrayControl.at(index).get('course')?.valueChanges
                .pipe(
                    startWith<string | any>(''),
                    map(value => {
                        return typeof value === 'string' ? value : value.name
                    }),
                    map(name => {
                        return name ? this._filterCourse(name) : this.courses?.slice()
                    })
                )
        }
    }

    public manageBatchControl(index: number) {
        const arrayControl = this.form.get('students') as FormArray;
        if (arrayControl) {
            this.filteredBatches[index] = arrayControl.at(index).get('batch')?.valueChanges
                .pipe(
                    startWith<string | any>(''),
                    map(value => {
                        return typeof value === 'string' ? value : value.name
                    }),
                    map(name => {
                        return name ? this._filterBatch(name) : this.batches?.slice()
                    })
                )
        }
    }

    // getting the students from the form as Form Array
    public get students(): FormArray {
        return this.form.get('students') as FormArray;
    }

    // remove student form with certain index
    public removeStudent(i:number): void {
        const controls = <FormArray>this.form.controls['students'];
        this.students.removeAt(i);
        this.filteredCourses?.splice(i, 1);
        this.filteredBatches?.splice(i, 1);
    }

    public onSubmit(): void {
        this.submit.emit(this.form.value);
    }

    public displayCourse(course: Course): string {
        return course && course.name ? course.name : '';
    }

    public displayBatch(batch: Batch): string {
        return batch && batch.name ? batch.name : '';
    }

    public ngOnInit(): void {
        this.addStudent();
    }

    private _filterCourse(name: string): Course[] | undefined {
        const filterValue = name.toLowerCase();

        return this.courses?.filter(option => option.name?.toLowerCase().indexOf(filterValue) === 0);
    }

    private _filterBatch(name: string): Batch[] | undefined {
        const filterValue = name.toLowerCase();

        return this.batches?.filter(option => option.name?.toLowerCase().indexOf(filterValue) === 0);
    }
}
