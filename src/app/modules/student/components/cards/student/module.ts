import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StudentCard } from '@talentlabs/app/app/modules/student/components/cards/student/student.card';

@NgModule({
    declarations: [StudentCard],
    imports: [
        CommonModule,
    ],
    exports: [StudentCard],
    entryComponents: [StudentCard]
})
export class StudentCardModule {
}
