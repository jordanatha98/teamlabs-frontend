import { Component, Inject, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Student } from '@talentlabs/app/models/student';
import { Form } from '@angular/forms';
import { Observable } from 'rxjs';
import { Course } from '@talentlabs/app/models/course';
import { StudentService } from '@talentlabs/app/services/student.service';
import { Batch } from '@talentlabs/app/models/batch';
import { StudentForm } from '@talentlabs/app/app/modules/student/components/forms/student-form/student.form';

@Component({
    selector: 'enroll-student-dialog',
    templateUrl: './enroll-student.dialog.html',
})
export class EnrollStudentDialog {
    @ViewChild('formTpl') public formTpl: StudentForm | undefined;

    public courses$!: Observable<Course[]>;
    public batches$: Observable<Batch[]> | undefined | null;

    public constructor(
        public dialogRef: MatDialogRef<EnrollStudentDialog>,
        @Inject(MAT_DIALOG_DATA) public data: Student,
        private service: StudentService,
    ) {
        this.courses$ = this.service.getCourses();
        this.batches$ = this.service.getBatches();
    }

    public onNoClick(): void {
        this.dialogRef.close();
    }

    public submitForm(): void {
        this.formTpl?.onSubmit();
        this.dialogRef.close(this.formTpl?.form.value);
    }

}