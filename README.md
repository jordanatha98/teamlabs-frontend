# TalentlabsFrontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.2.2.
Test project for Talentlabs.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you
change any of the source files.

## App Flow
Clone this repository and run `npm install` then after fininshed installing all of the packages, run `ng serve`
Open `http://localhost:4200` and will be redirected to Student Page in `localhost:4200/students/` then it will display
list of generated students that I have created. Click "+ Add Student" button to show the popup modal and fill the data neede.

I also added the functionality for adding multiple students data by clicking "+ add" button in the form and "x" to remove
the student data.

Note that after submiting the form, the data will be added to the list in the Student Page.

Also this project is responsive for mobile

## Running unit tests
I also added cypress unit test, testing the following flow:
1. Go to student page
2. Input student data
3. Input multiple student data
4. Input multiple student data, delete, and submit the form

