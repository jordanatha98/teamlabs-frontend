import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StudentService } from '../services/student.service';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { RippleHoverDirectiveModule } from '@talentlabs/app/directives/ripple/module';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { EnrollStudentDialog } from '@talentlabs/app/app/modules/student/dialogs/enroll-student/enroll-student.dialog';
import { StudentFormModule } from '@talentlabs/app/app/modules/student/components/forms/student-form/module';
import { MatRippleModule } from '@angular/material/core';
import { MatTooltipModule } from '@angular/material/tooltip';

@NgModule({
    declarations: [
        AppComponent,
        EnrollStudentDialog,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MatSidenavModule,
        MatCardModule,
        MatIconModule,
        MatDialogModule,
        RippleHoverDirectiveModule,
        StudentFormModule,
        MatRippleModule,
        MatTooltipModule,
    ],
    providers: [StudentService],
    bootstrap: [AppComponent],
    entryComponents: [EnrollStudentDialog]
})
export class AppModule {
}
