import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StudentForm } from '@talentlabs/app/app/modules/student/components/forms/student-form/student.form';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatIconModule } from '@angular/material/icon';
import { MatRippleModule } from '@angular/material/core';

@NgModule({
    declarations: [StudentForm],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatSelectModule,
        MatInputModule,
        MatAutocompleteModule,
        MatIconModule,
        MatRippleModule,
    ],
    exports: [StudentForm],
})
export class StudentFormModule {
}
