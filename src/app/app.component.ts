import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Subject, takeUntil } from 'rxjs';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent {
    public destroyed = new Subject<void>();
    public currentScreenSize: undefined | number;

    public displayNameMap = new Map([
        [Breakpoints.XSmall, {name: 'Xmall', num: 0}],
        [Breakpoints.Small, {name: 'Small', num: 0}],
        [Breakpoints.Medium, {name: 'Xmall', num: 1}],
        [Breakpoints.Large, {name: 'Xmall', num: 1}],
        [Breakpoints.XLarge, {name: 'Xmall', num: 1}],
    ]);

    constructor(breakpointObserver: BreakpointObserver) {
        breakpointObserver
            .observe([
                Breakpoints.XSmall,
                Breakpoints.Small,
                Breakpoints.Medium,
                Breakpoints.Large,
                Breakpoints.XLarge,
            ])
            .pipe(takeUntil(this.destroyed))
            .subscribe(result => {
                for (const query of Object.keys(result.breakpoints)) {
                    if (result.breakpoints[query]) {
                        this.currentScreenSize = this.displayNameMap.get(query)?.num ?? 0;
                    }
                }
            });
    }

    public ngOnDestroy(): void {
        this.destroyed.next();
        this.destroyed.complete();
    }
}
