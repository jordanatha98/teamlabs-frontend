import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EnrollStudentDialog } from '@talentlabs/app/app/modules/student/dialogs/enroll-student/enroll-student.dialog';
import { MatDialogModule } from '@angular/material/dialog';
import { StudentFormModule } from '@talentlabs/app/app/modules/student/components/forms/student-form/module';
import { StudentService } from '@talentlabs/app/services/student.service';

@NgModule({
    declarations: [EnrollStudentDialog],
    imports: [
        CommonModule,
        MatDialogModule,
        StudentFormModule
    ],
    exports: [EnrollStudentDialog],
    providers: [StudentService]
})
export class EnrollStudentDialogModule {
}
