describe('Students Page and Open Add Student', () => {
    it('Visits the student list page adn Clicks Add Student', () => {
        cy.visit('/students')
        cy.get('.add-student-button').click({ force: true })
    })
})
