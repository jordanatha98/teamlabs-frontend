import { ClassModel } from './class-model';

export class Batch extends ClassModel<Batch> {
    public id: number | undefined;
    public name: string | undefined;
}