import { NgModule } from '@angular/core';
import { RippleHoverDirective } from '@talentlabs/app/directives/ripple/ripple-hover';
import { CommonModule } from '@angular/common';


@NgModule({
    imports: [CommonModule],
    declarations: [RippleHoverDirective],
    exports: [RippleHoverDirective],
})
export class RippleHoverDirectiveModule {
}