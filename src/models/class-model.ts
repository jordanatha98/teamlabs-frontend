export abstract class ClassModel<T extends ClassModel = any> {
    public constructor(data?: Partial<T>) {
        Object.assign(this, data);
    }
}
