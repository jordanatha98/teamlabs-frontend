import { Component, Input } from '@angular/core';
import { Student } from '@talentlabs/app/models/student';

@Component({
    selector: 'student-card',
    templateUrl: './student.card.html',
    styleUrls: ['./student.card.scss']
})
export class StudentCard {
    @Input() public student: Student | undefined;

    public constructor() {
    }
}
