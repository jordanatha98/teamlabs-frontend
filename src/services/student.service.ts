import { Injectable } from '@angular/core';
import { Course } from '../models/course';
import { Batch } from '../models/batch';
import { Student } from '../models/student';
import { delay, Observable, of } from 'rxjs';

@Injectable()
export class StudentService {
    public courses: Course[] = [
        new Course({
            id: 1,
            name: 'Fundamentals of Javascript',
        }),
        new Course({
            id: 2,
            name: 'ReactJS how to',
        }),
        new Course({
            id: 3,
            name: 'Introduction to OOP',
        }),
        new Course({
            id: 4,
            name: 'Advanced Web Developer Bootcamp',
        }),
    ];

    public batches: Batch[] = [
        new Batch({
            id: 1,
            name: 'August 2022'
        }),
        new Batch({
            id: 2,
            name: 'September 2022'
        }),
        new Batch({
            id: 3,
            name: 'October 2022'
        }),
        new Batch({
            id: 4,
            name: 'November 2022'
        })
    ]

    public students: Student[] = [
        new Student({
            email: 'jordanatha98@gmail.com',
            fee: '200',
            course: this.courses[0],
            batch: this.batches[1],
        }),
        new Student({
            email: 'jordanathaa@gmail.com',
            fee: '300',
            course: this.courses[1],
            batch: this.batches[2],
        })
    ]

    // getting all the courses data
    public getCourses(): Observable<Course[]> {
        return of(this.courses)
    }

    // getting all the batches data
    public getBatches(): Observable<Batch[]> | undefined | null {
        return of(this.batches)
    }

    // getting all the students data
    public getStudents(): Observable<Student[]> {
        return of(this.students).pipe(
            delay(1000)
        )
    }
}