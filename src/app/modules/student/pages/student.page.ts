import { Component, OnDestroy, OnInit } from '@angular/core';
import { BehaviorSubject, first, Observable, Subject, takeUntil } from 'rxjs';
import { Student } from '@talentlabs/app/models/student';
import { StudentService } from '@talentlabs/app/services/student.service';
import { RippleRef } from '@angular/material/core';
import { MatDialog } from '@angular/material/dialog';
import { EnrollStudentDialog } from '@talentlabs/app/app/modules/student/dialogs/enroll-student/enroll-student.dialog';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';

@Component({
    selector: 'student-page',
    templateUrl: './student.page.html',
    styleUrls: ['./student.page.scss']
})
export class StudentPage implements OnInit, OnDestroy {
    public destroyed = new Subject<void>();
    public currentScreenSize: undefined | number;

    public displayNameMap = new Map([
        [Breakpoints.XSmall, {name: 'Xmall', num: 0}],
        [Breakpoints.Small, {name: 'Small', num: 0}],
        [Breakpoints.Medium, {name: 'Xmall', num: 1}],
        [Breakpoints.Large, {name: 'Xmall', num: 1}],
        [Breakpoints.XLarge, {name: 'Xmall', num: 1}],
    ]);

    public students$: Observable<Student[]>;
    public studentsSubject: BehaviorSubject<Student[]> = new BehaviorSubject<Student[]>([]);

    public rippleRef: undefined | RippleRef;

    public constructor(
        private service: StudentService,
        private dialog: MatDialog,
        breakpointObserver: BreakpointObserver,
    ) {
        this.students$ = this.studentsSubject.asObservable();
        breakpointObserver
            .observe([
                Breakpoints.XSmall,
                Breakpoints.Small,
                Breakpoints.Medium,
                Breakpoints.Large,
                Breakpoints.XLarge,
            ])
            .pipe(takeUntil(this.destroyed))
            .subscribe(result => {
                for (const query of Object.keys(result.breakpoints)) {
                    if (result.breakpoints[query]) {
                        this.currentScreenSize = this.displayNameMap.get(query)?.num ?? 0;
                    }
                }
            });
    }

    // opening dialog for the form
    public openDialog(): void {
        const dialogRef = this.dialog.open(EnrollStudentDialog, {
            width: this.currentScreenSize === 0 ? '75%' : '50%',
            height: '75%'
        });

        dialogRef.afterClosed().subscribe(result => {
            this.students$.pipe(
                first()
            ).subscribe(
                (students) => {
                    if (result) {
                        this.studentsSubject.next([...students, ...result.students]);
                    }
                }
            )
        });
    }

    // getting the students data
    public ngOnInit(): void {
        this.service.getStudents().subscribe(
            (students) => {
                this.studentsSubject.next(students)
            }
        )
    }

    public ngOnDestroy(): void {
        this.destroyed.next();
        this.destroyed.complete();
    }
}
