import { forwardRef } from '@angular/core';
import { Course } from './course';
import { Batch } from './batch';
import { ClassModel } from './class-model';
import { Type } from 'class-transformer';

export class Student extends ClassModel<Student> {
    public email: string | null | undefined;
    public fee: string | null | undefined;

    @Type(forwardRef(() => Course) as any)
    public course: Course | null | undefined;

    @Type(forwardRef(() => Batch) as any)
    public batch: Batch | null | undefined;
}