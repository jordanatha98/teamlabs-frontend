import { ClassModel } from './class-model';
import { Batch } from './batch';

export class Course extends ClassModel<Batch> {
    public id: number | undefined;
    public name: string | null | undefined;
}